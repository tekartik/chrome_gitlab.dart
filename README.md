# chrome_gitlab.dart

Travis helper scripts to allow running unit test on Chrome

## Setup

This assumes you are familiar with Dart and Gitlab integration

Include `chrome_gitlab.dart` as a development dependencies in your `pubspec.yaml` file

```yaml
dev_dependencies:
  test: any
  chrome_gitlab:
    git:
      url: git@gitlab.com:tekartik/chrome_gitlab.dart
      ref: dart3a
```

Create the following `.gitlab-ci.yml` file

```yaml
image: google/dart:latest

stages:
- test

before_script:
  - export PUB_CACHE=$PWD/.pub_cache/
  - dart --version
  - dart pub get
  # pre-compile
  - dart run chrome_gitlab:env_rc -h
  - source $(dart run chrome_gitlab:env_rc)

test:
  stage: test
  script:
    - pub run test -p vm,chrome
```

## Example

This project use the solution itself