import 'dart:io';

import 'package:args/args.dart';
import 'package:path/path.dart';

const String envRc = "env.rc";

main(List<String> args) async {
  Directory tempDir = await Directory.systemTemp.createTemp();

  ArgParser parser = ArgParser(allowTrailingOptions: true);
  parser.addFlag('help', abbr: 'h', help: 'Usage help', negatable: false);
  parser.addFlag('verbose',
      abbr: 'v', help: 'Display content in stderr', negatable: false);

  ArgResults results = parser.parse(args);
  if (results['help'] as bool) {
    stderr.writeln(parser.usage);
    return;
  }
  bool verbose = results['verbose'] as bool;

  String content = r"""
apt-get update
apt-get install -y gettext-base wget
echo 'deb http://dl.google.com/linux/chrome/deb/ stable main' > /etc/apt/sources.list.d/chrome.list
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
set -x && apt-get update && apt-get install -y xvfb google-chrome-stable
wget -q -O /usr/bin/xvfb-chrome https://bitbucket.org/atlassian/docker-node-chrome-firefox/raw/ff180e2f16ea8639d4ca4a3abb0017ee23c2836c/scripts/xvfb-chrome
ln -sf /usr/bin/xvfb-chrome /usr/bin/google-chrome
chmod 755 /usr/bin/google-chrome
""";
  if (verbose) {
    stderr.writeln(content);
  }

  File dst = File(join(tempDir.path, envRc));
  await dst.writeAsString(content);
  stdout.write(dst.path);
}
