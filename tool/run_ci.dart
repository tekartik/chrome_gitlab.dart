import 'package:dev_test/package.dart';
import 'package:process_run/shell.dart';

Future<void> main() async {
  await Shell().run('dart run chrome_gitlab:show_env');
  await packageRunCi('.');
}
