import 'dart:convert';
import 'dart:io';

import 'package:process_run/cmd_run.dart';

main() async {
  await runExecutableArguments('which', ['dart'], verbose: true);
  await runExecutableArguments('which', ['pub'], verbose: true);
  await runExecutableArguments('which', ['dart2js'], verbose: true);
  await runExecutableArguments('which', ['google-chrome'], verbose: true);
  await runExecutableArguments('which', ['firefox'], verbose: true);
  Map info = {};
  info['Platform.operatingSystem'] = Platform.operatingSystem;
  info['Directory.current'] = Directory.current.path;
  info['Platform.environment'] = Platform.environment;
  print(const JsonEncoder.withIndent('  ').convert(info));
}
